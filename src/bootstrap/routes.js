function load(app) {
    setApplicationRoutes(app);
}

function setApplicationRoutes(app) {
    const inventoryController = require("../controllers/inventory");
    app.use("/inventory", inventoryController);
}

module.exports = {
    load
}