const express = require('express');
const applicationRoutes = require("./bootstrap/routes");

const app = express();
applicationRoutes.load(app);

module.exports = app;