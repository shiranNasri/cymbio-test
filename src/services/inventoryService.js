const axios = require("axios");
const config = require("config");
const _ = require("lodash");
const SHOPIFY = config.SHOPIFY;

const token = Buffer.from(`${SHOPIFY.API_KEY}:${SHOPIFY.PASSWORD}`, 'utf8').toString('base64');
const requestHeaders = {
    Authorization: `Basic ${token}`
}

async function handleNextProducts(resHeaders) {
    try {
        const linkHeader = _.get(resHeaders, "link", null);
        const splitLink = linkHeader.split(",");
        let url;

        for (link of splitLink) {
            const match = link.match(/<(https:\/\/.*?)>; rel="next"/);
            if (match != null) {
                url = match[1];
                break;
            }
        }
        return await axios(url, { headers: requestHeaders });
    }
    catch (err) {
        console.log(err);
    }
}

function checkIfNextPageExist(resHeaders) {
    try {
        let linkHeader = _.get(resHeaders, "link", null);
        if (linkHeader) {
            if (linkHeader.search(`rel="next"`) != -1)
                return true;
        }
        return false;
    }
    catch (err) {
        console.log(err);
    }
}

function parseProducts(productsRes) {
    try {
        const products = _.get(productsRes, 'data.products', null);
        const parsedProducts = [];
        if (products) {
            for (product of products) {
                for (variant of product.variants) {
                    parsedProducts.push({ [variant.sku]: variant.inventory_quantity });
                }
            }
        }
        return parsedProducts;
    }
    catch (err) {
        console.log(err);
    }
}

async function getProducts() {
    try {
        let allProducts = [];
        let hasNextPage = false;

        let productsRes = await axios(`${SHOPIFY.URL_SHOPIFY}/products.json?limit=${SHOPIFY.LIMIT_PRODUCTS}&fields=variants`, { headers: requestHeaders });

        let parsedProduct = parseProducts(productsRes);
        allProducts = allProducts.concat(parsedProduct);

        hasNextPage = checkIfNextPageExist(productsRes.headers);
        while (hasNextPage) {
            productsRes = await handleNextProducts(productsRes.headers);
            parsedProduct = parseProducts(productsRes);
            allProducts = allProducts.concat(parsedProduct);
            hasNextPage = checkIfNextPageExist(productsRes.headers);
        }
        return allProducts;
    }
    catch (err) {
        console.log(err)
    }
}

module.exports = {
    getProducts
}