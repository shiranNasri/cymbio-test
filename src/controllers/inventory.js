const express = require("express");
const inventoryHandler = require("../services/inventoryService");
const router = express.Router();

router.get("/", async function (req, res) {
    try {
        const products = await inventoryHandler.getProducts();
        return res.status(200).send(products);
    }
    catch (err) {
        return res.status(500).send(err);
    }
});

module.exports = router;