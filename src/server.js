const app = require("./app");
const config = require('config');

const port = config.PORT || 3000;
const server = app.listen(port, () => {
  console.info(`App is running at http://${config.HOSTNAME}:${port}`);
});

module.exports = server;